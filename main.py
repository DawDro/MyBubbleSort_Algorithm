# main.py

# Arrays to be sorted:
arr1 = []
arr2 = [1,2,5,3,1,7,9,1,12,83,1,5,3,2]
arr3 = [1,1,1,1,1,1,1,2,1,1,1]
arr4 = [2,2,2,2]
arr5 = [1,2]
arr6 = [2,1]

# Script

def bubble_sort(arr):
    loops = len(arr)-1
    for n in range(loops):
        i = 0
        j = 1
        while j < len(arr):
            if arr[i] > arr[j]:
                temp_i = arr[i]
                temp_j = arr[j]
                arr[i] = temp_j
                arr[j] = temp_i
            else:
                i += 1
                j += 1
        print(arr)

bubble_sort(arr2)